/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web.db.controller;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.Persistence;
import web.db.entity.Page;

/**
 *
 * @author human
 */
public class PageController {

    private static final Logger LOG = Logger.getLogger(PageController.class.getName());
    private final EntityManagerFactory emf;

    public PageController() {
        this.emf = Persistence.createEntityManagerFactory("CMSPU");
    }

    public EntityManager getManager() {
        return emf.createEntityManager();
    }
    
    public Page getPageById(Integer id) {
        EntityManager manager = getManager();

        Page page = manager.find(Page.class, id);

        manager.close();
        
        return page;
    }

    public Page getPageByName(String pageName) {
        EntityManager manager = getManager();

        Page page = null;

        try {
            page = manager.createNamedQuery("Page.findByName", Page.class)
                    .setParameter("name", pageName)
                    .getSingleResult();
        } catch (NoResultException | NonUniqueResultException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }

        manager.close();
        
        return page;
    }

    public void updatePage(Page page) {
        EntityManager manager = getManager();

        manager.getTransaction().begin();
        manager.merge(page);
        manager.getTransaction().commit();

        manager.close();
    }
}
