/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import web.db.controller.PageController;
import web.db.entity.Page;

/**
 *
 * @author human
 */
public class PageServlet extends HttpServlet {
    
    private PageController controller;

    @Override
    public void init() throws ServletException {
        controller = new PageController();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String pageName = req.getParameter("name");
        
        if (pageName == null || pageName.isEmpty()) {
            pageName = "index";
        }
        
        Page foundPage = controller.getPageByName(pageName);
        
        if (foundPage == null) {
            req.setAttribute("pageName", pageName);
            resp.sendError(404);
            
            return;
        }
        req.setAttribute("page", foundPage);
        
        req.getRequestDispatcher("/WEB-INF/jsp/template.jsp").forward(req, resp);
    }
    
}
