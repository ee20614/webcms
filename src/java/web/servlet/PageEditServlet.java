/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import web.db.controller.PageController;
import web.db.entity.Page;

/**
 *
 * @author human
 */
public class PageEditServlet extends HttpServlet {

    private PageController controller;

    @Override
    public void init() throws ServletException {
        controller = new PageController();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String pageId = req.getParameter("id");

        Page foundPage = controller.getPageById(Integer.valueOf(pageId));

        req.setAttribute("editPage", foundPage);

        req.getRequestDispatcher("/WEB-INF/jsp/page.edit.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String pageId = req.getParameter("id");
        String name = req.getParameter("name");
        String title = req.getParameter("title");
        String content = req.getParameter("content");

        Page foundPage = controller.getPageById(Integer.valueOf(pageId));

        foundPage.setName(name);
        foundPage.setTitle(title);
        foundPage.setTemplate(content);

        controller.updatePage(foundPage);

        doGet(req, resp);
    }

}
