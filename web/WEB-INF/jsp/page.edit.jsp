<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="web.db.entity.Page"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Editing page</title>
    </head>
    <body>
        <h1>Page editing</h1>
        
        <div>
            <form method="post">
                <div>
                    <label for="fld_name">Name:</label><br />
                    <input id="fld_name" name="name" value="${editPage.name}" />
                </div>
                <div>
                    <label for="fld_title">Title:</label><br />
                    <input id="fld_title" name="title" value="${editPage.title}" />
                </div>
                <div>
                    <label for="fld_content">Content:</label><br />
                    <textarea id="fld_content" name="content">${editPage.content}</textarea>
                </div>
                <button>Save</button>
            </form>
        </div>
    </body>
</html>
