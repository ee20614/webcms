<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="web.db.entity.Page"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>NOT FOUND</title>
    </head>
    <body>
        <h1>THE PAGE "${pageName}" NOT FOUND!</h1>
    </body>
</html>
