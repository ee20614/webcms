<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="web.db.entity.Page"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><%=((Page)request.getAttribute("page")).getTitle()%></title>
    </head>
    <body>
        ${page.content}
    </body>
</html>
